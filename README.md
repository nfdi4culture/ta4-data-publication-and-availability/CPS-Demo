# Computational Publishing Service Demo

[Project issues](https://github.com/orgs/NFDI4Culture/projects/1/views/4) | [Wiki - Work plan](https://github.com/NFDI4Culture/CPS-Demo/wiki/Work-plan-and-existing-R&D)

## Prototype: Exhibition catalogue publication

Simon Worthington, [simon.worthington@tib.eu](mailto:simon.worthington@tib.eu), NFDI4Culture. Open Science Lab, TIB. 03.2024

Demo and code: 

GitHub [https://github.com/NFDI4Culture/CPS-Demo](https://github.com/NFDI4Culture/CPS-Demo)

GitLab https://nfdi4culture.gitlab.io/ta4-data-publication-and-availability/CPS-Demo/docs/

## The service

Computational Publishing Service (CPS) uses Jupyter Notebooks to publish from cultural digital collections using Linked Open Data, and Wikidata and Wikibase. The service is designed to sit on top of existing NFDI4Culture Linked Open Data infrastructure.

Use cases: Digital Humanities scholars, cultural digital collections, and humanities publishing.

Value proposition: Provide a channel to audiences for cultural digital collections, enable reusable machine readable record of content used in publications, allow humanities publishers to integrate and support computational publications.

## The prototype

The goal is to demonstrate automated multi-format publishing – web, PDF, ePub Docx, and  interactive Jupyter Notebooks – of content from digital cultural collections stored as Linked Open Data (LOD) on Wikidata and Wikibase, and from other APIs and LOD sources.

The Jupyter Notebooks will be used in a virtual online environment 'Github Codespace' so that there are __zero-install__ overheads as a barrier for users.

The publications will be authored using Jupyter Notebooks used via the Google Colab platform, from GitHub storage, and rendered to multi-format using the Jupyter Notebook wrapper platform Quarto. The publication will be published to GitHub.

The content of the demo will be a fictional exhibition catalogue of a collection of paintings from the Corpus of Baroque Ceiling Painting in Germany (CbDD).

![Workflow](images/cps-workflow.svg)

[Link to drawing](https://docs.google.com/drawings/d/1YOS5C4GYxoWCXuSEN1039IjGtbEpYJRa1vXCraEugts/edit?usp=sharing) on Google Drive (commenting view)

## Software Citation

Worthington, S., & Bowie, S. (2024). Computational Publishing Service Demo (Version 0.0.1a) [Computer software]. https://github.com/NFDI4Culture/CPS-Demo

Add your name and ORCID in the CITATION.cff file to be attributed for your code contributions. Make a pull request after editing.

Edit Citation File Format: [CITATION.cff](CITATION.cff)

About Citation File Format: https://citation-file-format.github.io/

## Licensing

License: MIT License http://www.apache.org/licenses/

License information: [LICENSE](LICENSE)

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a> This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.

